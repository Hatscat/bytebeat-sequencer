# Setup

## 1. download or clone this repo

```
curl -L https://gitlab.com/hatscat/bytebeat-sequencer/repository/master/archive.tar.gz | tar xz
```

#### or

```
git clone git@gitlab.com:hatscat/bytebeat-sequencer.git
```

> but you will need ssh key before cloning (step 2 and 3)...

## 2. install Docker and generate your personal SSH key to use gitlab repository

```
cd bytebeat-sequencer/dev-env-setup
sudo -v
./setup.sh
```

## 3. Copy your public key in your gitlab user settings

> [gitlab.com/profile/keys](https://gitlab.com/profile/keys)

## 4. build Docker image

```
./build-docker-image.sh <EMAIL> <USERNAME>
```

## 5. run Docker container as an interactive session

```
docker run -it --rm -p 3000:3000 -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix bytebeat-seq-env
```

> tip: make aliases :smile:

### my aliases

```
alias docker_clean_ps='docker rm $(docker ps --filter=status=exited --filter=status=created -q)'
alias docker_clean_images='docker rmi $(docker images -a --filter=dangling=true -q)'

alias build_bbs="cd ${HOME}/bytebeat-sequencer/dev-env-setup && ./build-docker-image.sh $EMAIL $USER"                                                                                
alias bbs="docker_clean_ps; docker_clean_images; build_bbs"                                      
alias rbs="docker run -it --rm -p 3000:3000 -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix bytebeat-seq-env"
```
