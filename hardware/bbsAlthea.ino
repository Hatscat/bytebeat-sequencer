// working draft!

#include <I2S.h>

// 1<<(t/1280%8)&101&&t*7&t/5 = 1<<(t/1280%8)&101?t*7&t/5:0
// ((p=1<<(t/2048%8))&97&&t*8&t/8)|(p&10&&t*6&t/8)|(p&4&&t*4)|(p&128&&t*12&t/8)

const int sampleRate = 8e3;
const int bufferSize = 0x100;
const int minVolume = 0;
const int maxVolume = 0x7FFF;
const int minPitch = 20;
const int maxPitch = 2;
const int analogMaxValue = 0x3FF;

const int volumePin = A0;
const int pitchPin = A1;

unsigned long t; // global

typedef struct {
   unsigned char pattern;
   int frequency;
   bool adsr;
} row;

void setup() {
  //Serial.begin(4800); // to test if it's slower
  Serial.begin(9600); // set the data rate in bits per second (baud) for serial data transmission
  
  //if (!I2S.begin(I2S_LEFT_JUSTIFIED_MODE, sampleRate, 16)) {
  if (!I2S.begin(I2S_PHILIPS_MODE, sampleRate, 16)) {
  //if (!I2S.begin(I2S_PHILIPS_MODE, sampleRate, 8)) {
    Serial.println("Failed to initialize I2S!");
    while (1);
  }
  
  t = 0;

  // TODO: setup 4 row struct
}

void loop() {
  // TODO: read all pins here
  int volume = map(analogRead(volumePin), 0, analogMaxValue, minVolume, maxVolume);
  int pitch = map(analogRead(pitchPin), 0, analogMaxValue, minPitch, maxPitch);
  
  //playSample(); // TODO: to remove
  playSamples(volume, pitch);
}

void playSamples(int volume, int pitch) {
  //((p=1<<(t/2048%8))&97&&t*8&t/8)|(p&10&&t*6&t/8)|(p&4&&t*4)|(p&128&&t*12&t/8)

  //unsigned char sampleBuffer[bufferSize];
  unsigned short sampleBuffer[bufferSize];
  int pulseDuration = pitch * 0x100;
  
  for (int i = 0; i < bufferSize; ++i) {
    unsigned int p = 1 << (t / pulseDuration & 7);
    unsigned char sample_1 = p & 97 ? t*8 & t/pitch : 0;
    unsigned char sample_2 = p & 10 ? t*6 & -t/pitch : 0;
    unsigned char sample_3 = p & 4 ? t*4 : 0;
    unsigned char sample_4 = p & 128 ? t*12 & t/pitch : 0;

  Serial.println(sample_1);
    //sampleBuffer[i] = sample_1 | sample_2 | sample_3 | sample_4;
    sampleBuffer[i] = map(sample_1 | sample_2 | sample_3 | sample_4, 0, 0xFF, 0, volume);
    
    ++t;
  }
  
  while (I2S.availableForWrite() < 2 * bufferSize);
  
  I2S.write(sampleBuffer, bufferSize);
  I2S.write(sampleBuffer, bufferSize);

  // DEBUG
  //Serial.print("t = ");
  //Serial.println(t);
  Serial.print("sampleBuffer = ");
  for (int i = 0; i < bufferSize; ++i) {
    Serial.print(" ");
    Serial.print(sampleBuffer[i]);
  }
  Serial.println("");

}


// TODO: to remove
void playSample() {
  unsigned char sample = 1<<(t/1280&7) & 101 ? t*7&t/5 : 0;
  ++t;
  
  while (I2S.availableForWrite() < 2);
  
  I2S.write(sample << 7);
  I2S.write(sample << 7);
  
  Serial.print("t = ");
  Serial.print(t);
  Serial.print(" | sample = ");
  Serial.println(sample);
}





