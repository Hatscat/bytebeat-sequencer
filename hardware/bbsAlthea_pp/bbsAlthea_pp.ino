// not tested yet!

#include <I2S.h>

const int sampleRate = 8e3;
const int bufferSize = 0x100;
const int minVolume = 0;
const int maxVolume = 0x7FFF;
const int minPitch = 40;
const int maxPitch = 8;
const int analogMaxValue = 0x3FF;

const int volumePin = A0;
const int pitchPin = A1;

typedef struct {
   unsigned char pattern;
   int frequency;
   bool adsr;
} row;

row row_1; // global
row row_2; // global
row row_3; // global
row row_4; // global

unsigned long t; // global

void setup() {
  //Serial.begin(4800); // to test if it's slower
  Serial.begin(9600); // set the data rate in bits per second (baud) for serial data transmission
  
  if (!I2S.begin(I2S_PHILIPS_MODE, sampleRate, 16)) {
    Serial.println("Failed to initialize I2S!");
    while (1);
  }
  
  t = 0;
}

void loop() {
  int volume = map(analogRead(volumePin), 0, analogMaxValue, minVolume, maxVolume);
  int pitch = map(analogRead(pitchPin), 0, analogMaxValue, minPitch, maxPitch);
  int pulseDuration = getPulseDuration(pitch);

  // TODO: read all other pins here
  // tmp test values: ((p=1<<(t/2048%8))&97&&t*8&t/8)|(p&10&&t*6&t/-8)|(p&4&&t*4&t/-8)|(p&128&&t*12&t/8)
  row_1.pattern = 97;
  row_2.pattern = 10;
  row_3.pattern = 4;
  row_4.pattern = 128;

  row_1.frequency = 8;
  row_2.frequency = 6;
  row_3.frequency = 4;
  row_4.frequency = 12;

  row_1.adsr = 0;
  row_2.adsr = 1;
  row_3.adsr = 1;
  row_4.adsr = 0;
  
  // TODO: light leds (green) at getCurrentColumn(pulseDuration)
  // TODO: light leds (blue) of each row at row.pattern & (1 << i)
  
  playSamples(volume, pitch, pulseDuration);
}

void playSamples(int volume, int pitch, int pulseDuration) {
  unsigned short sampleBuffer[bufferSize];
  
  for (int i = 0; i < bufferSize; ++i) {
    unsigned int p = 1 << getCurrentColumn(pulseDuration);
    unsigned char sample_1 = p & row_1.pattern ? t * row_1.frequency & (row_1.adsr ? -t : t) / pitch : 0;
    unsigned char sample_2 = p & row_2.pattern ? t * row_2.frequency & (row_2.adsr ? -t : t) / pitch : 0;
    unsigned char sample_3 = p & row_3.pattern ? t * row_3.frequency & (row_3.adsr ? -t : t) / pitch : 0;
    unsigned char sample_4 = p & row_4.pattern ? t * row_4.frequency & (row_4.adsr ? -t : t) / pitch : 0;

    sampleBuffer[i] = map(sample_1 | sample_2 | sample_3 | sample_4, 0, 0xFF, 0, volume);
    
    ++t;
  }
  
  while (I2S.availableForWrite() < 2 * bufferSize);
  
  I2S.write(sampleBuffer, bufferSize); // left
  I2S.write(sampleBuffer, bufferSize); // right

  // DEBUG
  //Serial.print("t = ");
  //Serial.println(t);
  Serial.print("sampleBuffer = ");
  for (int i = 0; i < bufferSize; ++i) {
    Serial.print(" ");
    Serial.print(sampleBuffer[i]);
  }
  Serial.println("");
}

int getPulseDuration(int pitch) {
  return pitch << 8; // because 256 values with 8 bits in a char
}

int getCurrentColumn(int pulseDuration) {
  return t / pulseDuration & 7;
}


