module App.Model exposing (Model, Row, Adsr(..), maxColsNb, maxRowsNb, emptyModel, emptyRow)

import Array exposing (Array)


type alias Model =
    { errorMsg : Maybe String
    , bytebeat : Maybe String
    , pitch : Float
    , rows : Array Row
    , rowsNb : Int
    , colsNb : Int
    , pulse : Int
    , pause : Bool
    }


type alias Row =
    { adsr : Adsr
    , frequency : Float
    , beats : Array Bool
    }


type Adsr
    = Plain
    | Smooth
    | Harsh


emptyModel : Model
emptyModel =
    { errorMsg = Nothing
    , bytebeat = Nothing
    , pitch = defaultPitch
    , rows = Array.repeat defaultRowsNb emptyRow
    , rowsNb = defaultRowsNb
    , colsNb = defaultColsNb
    , pulse = 0
    , pause = True
    }


emptyRow : Row
emptyRow =
    { adsr = Harsh
    , frequency = defaultFreq
    , beats = Array.repeat defaultColsNb False
    }


maxColsNb : Int
maxColsNb =
    32


maxRowsNb : Int
maxRowsNb =
    16


defaultPitch : Float
defaultPitch =
    5


defaultFreq : Float
defaultFreq =
    6


defaultAdsr : Adsr
defaultAdsr =
    Harsh


defaultRowsNb : Int
defaultRowsNb =
    4


defaultColsNb : Int
defaultColsNb =
    16
