module App.Msgs exposing (Msg(..))


type Msg
    = NoOp
    | ChangePitch String
    | ChangeRowsNb String
    | ChangeColsNb String
    | ChangeAdsr Int String
    | ChangeFrequency Int String
    | ChangeBeat Int Int
    | ChangePulse Int
    | ChangePause
    | Stop
    | ResetPulse
