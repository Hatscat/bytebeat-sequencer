module App.Service.BeatsRow exposing (updateRow, updateBeat, refreshGridCols, refreshGridRows, stringToAdsr, adsrToString)

import Array exposing (Array)
import App.Model exposing (Row, Model, Adsr(..), emptyRow)


updateRow : Array Row -> Int -> (Row -> Row) -> Array Row
updateRow rows rowId rowMap =
    let
        r =
            Array.get rowId rows
                |> Maybe.withDefault emptyRow
                |> rowMap
    in
        Array.set rowId r rows


updateBeat : Array Row -> Int -> Int -> (Bool -> Bool) -> Array Row
updateBeat rows rowId beatId beatMap =
    let
        b arr =
            Array.get beatId arr
                |> Maybe.withDefault False
                |> beatMap

        rowMap r =
            { r | beats = Array.set beatId (b r.beats) r.beats }
    in
        updateRow rows rowId rowMap


refreshGridRows : Model -> Model
refreshGridRows model =
    let
        diff =
            model.rowsNb - (Array.length model.rows)

        rows =
            if diff > 0 then
                Array.repeat diff emptyRow
                    |> Array.append model.rows
            else if diff < 0 then
                Array.slice 0 model.rowsNb model.rows
            else
                model.rows
    in
        { model | rows = rows }


refreshGridCols : Model -> Model
refreshGridCols model =
    let
        updateRowCols r =
            let
                diff =
                    model.colsNb - (Array.length r.beats)

                beats =
                    if diff > 0 then
                        Array.repeat diff False
                            |> Array.append r.beats
                    else if diff < 0 then
                        Array.slice 0 model.colsNb r.beats
                    else
                        r.beats
            in
                { r | beats = beats }
    in
        { model | rows = Array.map updateRowCols model.rows }


stringToAdsr : String -> Maybe Adsr
stringToAdsr str =
    case str of
        "Plain" ->
            Just Plain

        "Smooth" ->
            Just Smooth

        "Harsh" ->
            Just Harsh

        _ ->
            Nothing


adsrToString : Adsr -> String
adsrToString adsr =
    case adsr of
        Plain ->
            "Plain"

        Smooth ->
            "Smooth"

        Harsh ->
            "Harsh"
