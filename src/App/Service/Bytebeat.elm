module App.Service.Bytebeat exposing (setBytebeat, pulse)

import Array exposing (Array)
import App.Model exposing (Row, Adsr(..), Model)


setBytebeat : Model -> Model
setBytebeat model =
    let
        rows =
            rowsWithBeats model.rows

        rowCount =
            Array.length rows

        pulseStr =
            bytebeatPulse model (rowCount > 1)

        beatRows =
            Array.toList rows
                |> List.indexedMap (bytebeatRow model.pitch)
                |> String.concat

        bytebeat =
            case rowCount of
                0 ->
                    Nothing

                1 ->
                    Just <|
                        String.concat
                            [ pulseStr
                            , beatRows
                            ]

                _ ->
                    Just <|
                        String.concat
                            [ "("
                            , pulseStr
                            , beatRows
                            , ")"
                            ]
    in
        { model | bytebeat = bytebeat }


rowsWithBeats : Array Row -> Array Row
rowsWithBeats rows =
    let
        doesRowCount row =
            rowBeatsCount row > 0
    in
        Array.filter doesRowCount rows


rowBeatsCount : Row -> Int
rowBeatsCount row =
    Array.map
        (\beat ->
            if beat then
                1
            else
                0
        )
        row.beats
        |> Array.foldl (+) 0


pulse : Float -> Int -> Int -> Int
pulse pitch colsNb t =
    let
        p =
            round (pitch * 256)
    in
        t // p % colsNb


bytebeatPulse : Model -> Bool -> String
bytebeatPulse model isVar =
    let
        p =
            model.pitch * 256 |> toString

        c =
            model.colsNb |> toString
    in
        if isVar then
            String.concat
                [ "(p=1<<(t/"
                , p
                , "%"
                , c
                , "))"
                ]
        else
            String.concat
                [ "1<<(t/"
                , p
                , "%"
                , c
                , ")"
                ]


bytebeatRow : Float -> Int -> Row -> String
bytebeatRow pitch rowId row =
    if rowBeatsCount row == 0 || row.frequency == 0 then
        ""
    else
        let
            var =
                if rowId > 0 then
                    ")|(p"
                else
                    ""

            pattern =
                bytebeatReversePattern (Array.toList row.beats |> List.reverse) |> toString

            freq =
                if row.frequency /= 1 then
                    "*" ++ (toString row.frequency)
                else
                    ""

            adsr =
                case row.adsr of
                    Plain ->
                        ""

                    Smooth ->
                        "&t/" ++ (toString pitch)

                    Harsh ->
                        "&t/-" ++ (toString pitch)
        in
            String.concat [ var, "&", pattern, "&&t", freq, adsr ]


bytebeatReversePattern : List Bool -> Int
bytebeatReversePattern beats =
    let
        b2n b p =
            if b then
                2 ^ p
            else
                0
    in
        case beats of
            b :: xs ->
                bytebeatReversePattern xs + b2n b (List.length xs)

            [] ->
                0
