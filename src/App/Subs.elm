port module App.Subs exposing (subscriptions)

import App.Msgs exposing (..)
import App.Model exposing (Model)


port changePulse : (Int -> msg) -> Sub msg


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch [ changePulse ChangePulse ]
