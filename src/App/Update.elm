port module App.Update exposing (update)

import Utils
import App.Msgs exposing (..)
import App.Model exposing (Model, maxColsNb, maxRowsNb, emptyModel)
import App.Service.BeatsRow exposing (updateRow, updateBeat, refreshGridCols, refreshGridRows, stringToAdsr)
import App.Service.Bytebeat exposing (setBytebeat, pulse)


port sendBytebeat : Maybe String -> Cmd msg


port resetPulse : () -> Cmd msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    action msg { model | errorMsg = Nothing }


action : Msg -> Model -> ( Model, Cmd Msg )
action msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        ChangePitch pitch ->
            let
                p =
                    String.toFloat pitch
                        |> Result.withDefault model.pitch
                        |> max 1
                        |> Utils.toFixed 2

                newModel =
                    setBytebeat { model | pitch = p }
            in
                ( newModel, sendBytebeatIfPlay newModel )

        ChangeRowsNb rowsNb ->
            let
                n =
                    String.toInt rowsNb
                        |> Result.withDefault model.rowsNb
                        |> clamp 1 maxRowsNb

                newModel =
                    (refreshGridRows >> setBytebeat) { model | rowsNb = n }
            in
                ( newModel, sendBytebeatIfPlay newModel )

        ChangeColsNb colsNb ->
            let
                n =
                    String.toInt colsNb
                        |> Result.withDefault model.colsNb
                        |> clamp 1 maxColsNb

                newModel =
                    (refreshGridCols >> setBytebeat) { model | colsNb = n }
            in
                ( newModel, sendBytebeatIfPlay newModel )

        ChangeAdsr rowId adsr ->
            let
                rowMap row =
                    { row
                        | adsr =
                            stringToAdsr adsr
                                |> Maybe.withDefault row.adsr
                    }

                rows =
                    updateRow model.rows rowId rowMap

                newModel =
                    setBytebeat { model | rows = rows }
            in
                ( newModel, sendBytebeatIfPlay newModel )

        ChangeFrequency rowId freq ->
            let
                rowMap row =
                    { row
                        | frequency =
                            String.toFloat freq
                                |> Result.withDefault row.frequency
                                |> Utils.toFixed 2
                    }

                rows =
                    updateRow model.rows rowId rowMap

                newModel =
                    setBytebeat { model | rows = rows }
            in
                ( newModel, sendBytebeatIfPlay newModel )

        ChangeBeat rowId beatId ->
            let
                rows =
                    updateBeat model.rows rowId beatId (\b -> not b)

                newModel =
                    setBytebeat { model | rows = rows }
            in
                ( newModel, sendBytebeatIfPlay newModel )

        ChangePulse t ->
            ( { model | pulse = pulse model.pitch model.colsNb t }, Cmd.none )

        ChangePause ->
            let
                pause =
                    not model.pause

                bytebeat =
                    if pause then
                        Nothing
                    else
                        model.bytebeat
            in
                ( { model | pause = pause }, sendBytebeat bytebeat )

        Stop ->
            ( emptyModel, sendBytebeat Nothing )

        ResetPulse ->
            ( model, resetPulse () )


sendBytebeatIfPlay : Model -> Cmd msg
sendBytebeatIfPlay model =
    if model.pause then
        Cmd.none
    else
        sendBytebeat model.bytebeat
