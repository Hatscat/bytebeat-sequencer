module App.View exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Array
import Utils
import App.Msgs exposing (..)
import App.Model exposing (Model, Adsr(..), emptyRow)
import App.Service.BeatsRow exposing (adsrToString)


view : Model -> Html Msg
view model =
    let
        bytebeat =
            Maybe.withDefault "" model.bytebeat

        bbEditorUrl =
            "http://wry.me/bytebeat/?code0="
    in
        div []
            [ div [ class "hero is-primary has-text-centered" ]
                [ div [ class "hero-body" ]
                    [ h1 [ class "title" ] [ text "Bytebeat Sequencer" ]
                    ]
                ]
            , div [ class "container" ]
                [ div [ class "box" ]
                    [ settingsLevel model
                    , div [ class "columns" ]
                        [ adsrColumn model.rowsNb
                        , freqColumn model
                        , beatsColumn model
                        ]
                    , div [ class "hero has-text-centered" ]
                        [ div [ class "hero-body" ]
                            [ a
                                [ class "subtitle has-text-link"
                                , href (bbEditorUrl ++ (Http.encodeUri bytebeat))
                                , target "blank"
                                ]
                                [ text bytebeat ]
                            ]
                        ]
                    ]
                ]
            ]


settingsLevel : Model -> Html Msg
settingsLevel model =
    div [ class "level" ]
        [ div [ class "level-left" ]
            [ div [ class "level-item is-horizontal" ]
                [ div [ class "field-label" ] [ label [ class "label" ] [ text "Pitch" ] ]
                , div [ class "field-body" ]
                    [ input
                        [ class "input is-primary"
                        , type_ "number"
                        , value (toString model.pitch)
                        , onInput ChangePitch
                        ]
                        []
                    ]
                ]
            ]
        , div [ class "level-right" ]
            [ div [ class "level-item is-horizontal" ]
                [ div [ class "field-label" ] [ label [ class "label" ] [ text "Rows" ] ]
                , div [ class "field-body" ]
                    [ input
                        [ class "input is-warning"
                        , type_ "number"
                        , value (toString model.rowsNb)
                        , onInput ChangeRowsNb
                        ]
                        []
                    ]
                ]
            , div [ class "level-item is-horizontal" ]
                [ div [ class "field-label" ] [ label [ class "label" ] [ text "Cols" ] ]
                , div [ class "field-body" ]
                    [ input
                        [ class "input is-warning"
                        , type_ "number"
                        , value (toString model.colsNb)
                        , onInput ChangeColsNb
                        ]
                        []
                    ]
                ]
            ]
        ]


adsrColumn : Int -> Html Msg
adsrColumn rowsNb =
    let
        rowMap r =
            div [ class "field" ]
                [ div [ class "select is-primary is-expanded" ]
                    [ select [ Utils.onChangeValue (ChangeAdsr r) ]
                        [ option [ value (adsrToString Harsh) ] [ text "◣" ]
                        , option [ value (adsrToString Smooth) ] [ text "◢" ]
                        , option [ value (adsrToString Plain) ] [ text "■" ]
                        ]
                    ]
                ]
    in
        List.range 0 (rowsNb - 1)
            |> List.map rowMap
            |> (::)
                (div [ class "content" ]
                    [ label [ class "label has-text-centered" ] [ text "ADSR" ] ]
                )
            |> div [ class "column is-1" ]


freqColumn : Model -> Html Msg
freqColumn model =
    let
        rowMap r =
            div [ class "field" ]
                [ input
                    [ class "input is-primary"
                    , type_ "number"
                    , value (Array.get r model.rows |> Maybe.withDefault emptyRow |> .frequency |> toString)
                    , onInput (ChangeFrequency r)
                    ]
                    []
                ]
    in
        List.range 0 (model.rowsNb - 1)
            |> List.map rowMap
            |> (::)
                (div [ class "content" ]
                    [ label [ class "label has-text-centered" ] [ text "Freq" ] ]
                )
            |> div [ class "column is-1" ]


beatsColumn : Model -> Html Msg
beatsColumn model =
    let
        beat c r =
            Array.get r model.rows
                |> Maybe.withDefault emptyRow
                |> .beats
                |> Array.get c
                |> Maybe.withDefault False

        rowMap c r =
            let
                btColor =
                    if beat c r then
                        "primary"
                    else
                        "dark"

                bgColor =
                    if model.bytebeat /= Nothing && model.pulse == c then
                        "warning"
                    else
                        "light"
            in
                div [ class ("field has-background-" ++ bgColor) ]
                    [ button
                        [ class ("button is-rounded is-" ++ btColor)
                        , onClick (ChangeBeat r c)
                        ]
                        []
                    ]

        colMap c =
            List.range 0 (model.rowsNb - 1)
                |> List.map (rowMap c)
                |> div [ class "column has-text-centered" ]
    in
        div [ class "column is-10" ]
            [ musicControls model.pause
            , List.range 0 (model.colsNb - 1)
                |> List.map colMap
                |> div [ class "columns is-gapless" ]
            ]


musicControls : Bool -> Html Msg
musicControls isPause =
    let
        playPauseIcon =
            if isPause then
                "play"
            else
                "pause"
    in
        div [ class "buttons has-addons is-centered" ]
            [ span [ class "button icon", onClick ResetPulse ]
                [ i [ class "fas fa-step-backward" ] [] ]
            , span [ class "button icon", onClick ChangePause ]
                [ i [ class ("fas fa-" ++ playPauseIcon) ] [] ]
            , span [ class "button icon", onClick Stop ]
                [ i [ class "fas fa-stop" ] [] ]
            ]
