module Main exposing (..)

import Html
import App.Model exposing (Model, emptyModel)
import App.View exposing (view)
import App.Update exposing (update)
import App.Subs exposing (subscriptions)
import App.Msgs exposing (Msg, Msg(NoOp))


main : Program Never Model Msg
main =
    Html.program
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }


init : ( Model, Cmd Msg )
init =
    update NoOp emptyModel
