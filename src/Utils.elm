module Utils exposing (viewIf, toFixed, onChangeValue)

import Html exposing (..)
import Html.Events exposing (..)
import Json.Decode


viewIf : Bool -> Html msg -> Html msg
viewIf condition content =
    if condition then
        content
    else
        Html.text ""


toFixed : Int -> Float -> Float
toFixed precision value =
    let
        power =
            toFloat 10 ^ (toFloat precision)
    in
        (round (value * power) |> toFloat)
            / power


onChangeValue : (String -> msg) -> Attribute msg
onChangeValue message =
    on "change" (Json.Decode.map message (Json.Decode.at [ "target", "value" ] Json.Decode.string))
