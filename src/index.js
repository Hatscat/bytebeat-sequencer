// @format

import 'bulma/css/bulma.min.css';
import { Main } from './Main.elm';
import registerServiceWorker from './registerServiceWorker';

/** Audio Context Setup */
let scriptProcessor = null;
let bytebeatFunc = null;
let audioCtx = null;
let t = 0;
let isAudioNodeConnected = false;

let isAudioStarted = false;
function startAudio() {
  if (isAudioStarted) return;
  else isAudioStarted = true;

  audioCtx = new AudioContext();
  const bufferSize = 1 << 8;
  const sampleRate = 8e3 / audioCtx.sampleRate;
  scriptProcessor = audioCtx.createScriptProcessor(bufferSize, 0, 1);
  
  scriptProcessor.onaudioprocess = function(e) {
    const channelData = e.outputBuffer.getChannelData(0);

    if (bytebeatFunc) {
      for (let i = 0; i < bufferSize; i++) {
        t += sampleRate;
        channelData[i] = (bytebeatFunc(t) % bufferSize) / 255;
      }
      bytebeatApp.ports.changePulse.send(t | 0);
    } else {
      t = 0;
      channelData.fill(0);
    }
  };
}
addEventListener('click', startAudio);

/** Elm App Setup */
const bytebeatApp = Main.embed(document.getElementById('root'));

/** Elm App Ports */
bytebeatApp.ports.sendBytebeat.subscribe(function(bytebeat) {
  if (bytebeat) {
    bytebeatFunc = eval('(t,p)=>' + bytebeat);

    if (scriptProcessor && audioCtx && !isAudioNodeConnected) {
      isAudioNodeConnected = true;
      scriptProcessor.connect(audioCtx.destination);
    }
  } else {
    bytebeatFunc = null;

    if (scriptProcessor && audioCtx && isAudioNodeConnected) {
      isAudioNodeConnected = false;
      scriptProcessor.disconnect(audioCtx.destination);
    }
  }
});

bytebeatApp.ports.resetPulse.subscribe(function() {
  t = 0;
});

/** Service Worker Setup */
registerServiceWorker();
